Cooking Recipe Shopping Site <br>
This project was generated with Angular CLI version 6.0.0.

Reason for picking the MIT Licence  <br> 
To demonstratie the freedom and the scope of my personal project. Anybody can use this project to add, edit, and remove anything they want.

Setup Environment  <br>
Run npm install for installing angular node_modules files neccssary to run this application

Development server  <br>
Run ng serve for a dev server. Navigate to http://localhost:4200/. The app will automatically reload if you change any of the source files.

Code scaffolding  <br>
Run ng generate component component-name to generate a new component. You can also use ng generate directive|pipe|service|class|guard|interface|enum|module.

Build  <br>
Run ng build to build the project. The build artifacts will be stored in the dist/ directory. Use the --prod flag for a production build.

Running unit tests  <br>
Run ng test to execute the unit tests via Karma.

Running end-to-end tests  <br>
Run ng e2e to execute the end-to-end tests via Protractor.

Further help  <br>
To get more help on the Angular CLI use ng help or go check out the Angular CLI README.
